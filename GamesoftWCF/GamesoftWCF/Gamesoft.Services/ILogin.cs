﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GamesoftWCF
{
 [ServiceContract]
    public interface ILogin
    {

        [OperationContract]
        bool Login(string _userName, string _password);
    }

}
