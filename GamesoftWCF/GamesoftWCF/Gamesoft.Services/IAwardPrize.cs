﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace GamesoftWCF.Gamesoft.Services
{
    [ServiceContract]
    public interface IAwardPrize
    {
        [OperationContract]
        int GetUser();
    }
}
