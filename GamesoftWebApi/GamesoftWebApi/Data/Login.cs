﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GamesoftWebApi.Models;


namespace GamesoftWebApi.Data
{
    public class Login : ILoginService
    {
        public async Task<User> LoginAsync(string userName, string password)
        {
            using (var dbContext = new Softcrylic_GamesoftEntities())
            {
                var _task = Task.Run(() => dbContext.Users.Where(a => a.Username == userName && a.Password == password).FirstOrDefault());
                await _task;
                return _task.Result;
            }
        }
    }
}