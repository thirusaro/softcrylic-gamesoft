﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamesoft.Services
{
    interface ILogin
    {
        bool Login();
    }
}
