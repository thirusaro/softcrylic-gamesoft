﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GamesoftWCF;
using GamesoftWCF.Gamesoft.Services;


namespace Gamesoft.Test
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void ValidUserLogin()
        {
            ILogin _obj = new LoginService();
            bool isValidUser = _obj.Login("Ram", "RamPassword");
            Assert.IsTrue(isValidUser);
        }

        [TestMethod]
        public void InValidUserLogin()
        {
            ILogin _obj = new LoginService();
            bool isValidUser = _obj.Login("Ram", "RamPassword123");
            Assert.IsFalse(isValidUser);
        }
    }
}
