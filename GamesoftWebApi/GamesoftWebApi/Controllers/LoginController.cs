﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GamesoftWebApi.Models;
using GamesoftWebApi.Data;

namespace GamesoftWebApi.Controllers
{
    public class LoginController : ApiController
    {
        // GET api/values
        public IHttpActionResult Get(string _userName, string _password)
        {
            try
            {
                ILoginService _service = new Login();
                var _user = _service.LoginAsync(_userName, _password);
                _user.Wait();
                if (_user.Result == null)
                    return NotFound();
                return Ok(_user.Result);

            }
            catch
            {
                throw;
            }
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
