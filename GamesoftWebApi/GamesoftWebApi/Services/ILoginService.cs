﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GamesoftWebApi.Models;
using System.Threading.Tasks;

namespace GamesoftWebApi
{
    public interface ILoginService
    {
        Task<User> LoginAsync(string userName, string password);
    }
}