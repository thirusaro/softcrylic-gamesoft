﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using GamesoftWCF.Gamesoft.Services;
using GamesoftWCF.Gamesoft.Data;

namespace GamesoftWCF
{
 public class AwardService : IAwardPrize
    {
        public int GetUser()
        {
            try
            {
                using (var dbContext = new Softcrylic_GamesoftEntities())
                {
                    return dbContext.Users.OrderBy(a => Guid.NewGuid()).FirstOrDefault().UserID;
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
